### 1. Add git submodule

`git submodule add git@tiger.bitbucket.org:tiger-workshop/coreui-pro-custom-themes.git resources/theme`

### 2. Install theme modules 

`cd resources/theme`
`./npm-install-all.sh`

### 3. Copy templates/package.json to project root and install

`npm install`

### 4. Clear bower.json dependencies, left project specific modules

`rm -rf public_html/components` 
`bower install`

### 5. Update layout.blade.php

- Replace in all views: CSS class `card-block` to `card-body`

- Add `sidebar-lg-show` css to `<body>`

- Replace `<header>` menu button

```
<button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
</button>

<button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
</button>
```

- Update `<link href="{{ mix('css/site.css') }}" rel="stylesheet">` and `<script src="{!! mix('js/site.js') !!}"></script>` 

- Add `<script src="{!! asset('js/app.js') !!}"></script>` if not exist

### 6. Update login.blade.php and error pages with new site.css/site.js

### 7. (Optional) Create `resources/custom.scss` for project-based customization

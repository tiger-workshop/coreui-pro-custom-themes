'use strict';

/**
 * Pace Progress
 */
Pace.options.ajax.trackMethods = ['GET', 'POST'];
Pace.options.restartOnRequestAfter = 0;

/**
 * Datatables
 */

$.fn.dataTable.ext.errMode = 'none';

/**
 * Toastr
 */

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "1000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
}

$(document).ready(function($){
    /**
     *  Datetime pickers
     */
    $('body')
        .on('click', 'input.datetimepicker, input.show-datetime-picker', function () {
            if (typeof $(this).data("daterangepicker") === 'undefined') {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    timePicker: true,
                    timePickerSeconds: true,
                    timePicker24Hour: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-MM-DD HH:mm:ss'
                    }
                });
            }
            $(this).data("daterangepicker").show();
        })
        .on('click', 'input.datepicker, input.show-date-picker', function () {
            if (typeof $(this).data("daterangepicker") === 'undefined') {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                });
            }
            $(this).data("daterangepicker").show();
        })
        .on('click', 'input.timepicker, input.show-time-picker', function () {
            if (typeof $(this).data("daterangepicker") === 'undefined') {
                $(this).daterangepicker({
                    singleDatePicker: true,
                    timePicker: true,
                    timePickerSeconds: true,
                    timePicker24Hour: true,
                    locale: {
                        format: 'HH:mm:ss'
                    }
                });
            }
            $(this).data("daterangepicker").container.find('.calendar-table').hide();
            $(this).data("daterangepicker").show();
        })
        .on('click', 'input.datetimerangepicker, input.show-datetime-range-picker', function () {
            if (typeof $(this).data("daterangepicker") === 'undefined') {
                $(this).daterangepicker({
                    timePicker: true,
                    timePickerSeconds: true,
                    timePicker24Hour: true,
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-MM-DD HH:mm:ss'
                    }
                });
            }
            $(this).data("daterangepicker").show();
        })
        .on('click', 'input.daterangepicker, input.show-date-range-picker', function () {
            if (typeof $(this).data("daterangepicker") === 'undefined') {
                $(this).daterangepicker({
                    showDropdowns: true,
                    locale: {
                        format: 'YYYY-MM-DD'
                    }
                });
            }
            $(this).data("daterangepicker").show();
        });

    /**
     * Select2
     */

    $('.select2').select2({
        theme: 'bootstrap'
    });

    $('.select2.lock-on-select').on('select2:select', function (e)  {
        $(this).attr("readonly", true);
    });

    /**
     * CSRF Token
     */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
});
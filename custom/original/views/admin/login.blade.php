<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">

    <title>{{ Setting::get('site-name-' . App::getLocale()) }}</title>

    <!-- Main styles for this application -->
    <link href="{!! asset('css/site.css') !!}" rel="stylesheet">

</head>

<body class="app flex-row align-items-center">
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-6">
            <div class="card p-4">
                <div class="card-block p-2">
                    <form action="{!! route('admin.login') !!}" method="post">
                        {{ csrf_field() }}
                        <h1 style="text-align: center;">{{ Setting::get('company-name-' . App::getLocale()) }}</h1>
                        <p class="text-muted" style="text-align: center;">{{ Setting::get('site-name-' . App::getLocale()) }}</p>

                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                {{ $errors->first() }}
                            </div>
                        @endif

                        <div class="input-group mb-1">
                            <span class="input-group-text"><i class="icon-user"></i></span>
                            <input type="text" class="form-control" name="username" placeholder="{{ trans('backend.field.username') }}">
                        </div>
                        <div class="input-group mb-2">
                            <span class="input-group-text"><i class="icon-lock"></i></span>
                            <input type="password" class="form-control" name="password" placeholder="{{ trans('backend.field.password') }}">
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-success">{{ trans('backend.button.login') }}</button>
                            <!--<button type="button" class="btn btn-primary px-2">{{ trans('backend.button.forgot_password') }}</button>-->
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


<!-- Bootstrap and necessary plugins -->
<script src="{!! asset('components/jquery/dist/jquery.js') !!}"></script>
<script src="{!! asset('components/tether/dist/js/tether.js') !!}"></script>
<script src="{!! asset('components/bootstrap/dist/js/bootstrap.js') !!}"></script>
<script src="{!! asset('components/pace/pace.js') !!}"></script>

</body>

</html>
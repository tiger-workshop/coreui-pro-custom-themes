<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="shortcut icon" href="{{ asset('images/favicon.png') }}">
    <meta name="theme-color" content="#4A4C7D" />

    <title>{{ Setting::get('site-name-' . App::getLocale()) }}</title>

    <link href="{!! mix('css/site.css') !!}" rel="stylesheet">

</head>

<body class="guest guest-theme-{{ Config::get('times.theme') }}">

<div id="header">
    <h1 class="logo">{{ Setting::get('site-name') }}</h1>
</div>

<hr id="header-separator"/>

<div class="container">
    <div class="box">
        <div id="main">
            <form action="{!! route('admin.login') !!}" method="post" id="login-form" autocomplete="off">
                <h2>{{ Setting::get('company-name-' . App::getLocale()) }}</h2>
                <h3>{{ Setting::get('site-name-' . App::getLocale()) }}</h3>

                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        {{ $errors->first() }}
                    </div>
                @endif

                <fieldset>
                    <div class="form-group">
                        <input type="text" id="username" name="username" value="" class="form-control" placeholder="{{ trans('backend.field.username') }}"/>
                    </div>
                    <div class="form-group">
                        <input type="password" id="password" name="password" class="form-control" placeholder="{{ trans('backend.field.password') }}" />
                    </div>
                    {{ csrf_field() }}
                    <div class="buttons">
                        <button type="submit" class="btn btn-times">{{ trans('backend.button.login') }}</button>
                    </div>
                </fieldset>
            </form>
        </div>
    </div>
</div>

</body>

</html>
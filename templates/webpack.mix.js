const mix = require('laravel-mix');

mix.setPublicPath('public_html');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

const theme = 'tec';

mix.sass('resources/theme/custom/' + theme + '/theme.scss', 'css', {
    includePaths: ["resources/theme/coreui"]
});

mix.sass('resources/theme/fonts/fonts.scss', 'css');

// mix.sass('resources/custom.scss', 'css');

mix.styles([
    /**
     *  CoreUI Modules
     */
    'resources/theme/coreui/node_modules/datatables.net-bs4/css/dataTables.bootstrap4.css',
    'resources/theme/coreui/node_modules/ladda/dist/ladda.min.css',
    'resources/theme/coreui/node_modules/perfect-scrollbar/css/perfect-scrollbar.css',
    'resources/theme/coreui/src/vendors/pace-progress/css/pace.css',
    'resources/theme/coreui/src/vendors/bootstrap-daterangepicker/css/daterangepicker.css',
    'resources/theme/coreui/src/vendors/toastr/css/toastr.css',
    'resources/theme/coreui/src/vendors/select2/css/select2.css',

    /**
     *  Extra Modules
     */
    'node_modules/datatables.net-responsive-bs4/css/responsive.bootstrap4.css',
    'node_modules/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.css',
    'node_modules/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.css',
    'node_modules/datatables.net-buttons-bs4/css/buttons.bootstrap4.css',
    'node_modules/datatables-searchheader/dataTables.searchHeader.css',
    'node_modules/css-progress-wizard/css/progress-wizard.min.css',

    /**
     *  Custom Modules
     */

    /**
     *  CoreUI Main Styles
     */
    'public_html/css/theme.css',
    'public_html/css/fonts.css',
    'public_html/css/custom.css'
], 'public_html/css/site.css');

mix.scripts([
    /**
     *  CoreUI Modules
     */
    'resources/theme/coreui/node_modules/jquery/dist/jquery.js',
    'resources/theme/coreui/node_modules/popper.js/dist/umd/popper.js',
    'resources/theme/coreui/node_modules/bootstrap/dist/js/bootstrap.js',
    'resources/theme/coreui/node_modules/moment/moment.js',
    'resources/theme/coreui/node_modules/pace-progress/pace.js',
    'resources/theme/coreui/node_modules/datatables.net/js/jquery.dataTables.js',
    'resources/theme/coreui/node_modules/bootstrap-daterangepicker/daterangepicker.js',
    'resources/theme/coreui/node_modules/toastr/toastr.js',
    'resources/theme/coreui/node_modules/spin.js/spin.js',
    'resources/theme/coreui/node_modules/ladda/js/ladda.js',
    'resources/theme/coreui/node_modules/perfect-scrollbar/dist/perfect-scrollbar.js',
    'resources/theme/coreui/node_modules/select2/dist/js/select2.full.js',
    'resources/theme/coreui/node_modules/chart.js/dist/Chart.js',
    'resources/theme/coreui/node_modules/@coreui/coreui-pro/dist/js/coreui.js',

    /**
     *  Extra Modules
     */
    'node_modules/datatables.net-bs4/js/dataTables.bootstrap4.js',
    'node_modules/datatables.net-responsive/js/dataTables.responsive.js',
    'node_modules/datatables.net-responsive-bs4/js/responsive.bootstrap4.js',
    'node_modules/datatables.net-rowreorder/js/dataTables.rowReorder.js',
    'node_modules/datatables.net-fixedheader/js/dataTables.fixedHeader.js',
    'node_modules/datatables.net-buttons/js/dataTables.buttons.js',
    'node_modules/datatables.net-buttons-bs4/js/buttons.bootstrap4.js',
    'node_modules/datatables.net-buttons/js/buttons.colVis.js',
    'node_modules/datatables-searchheader/dataTables.searchHeader.js',

    /**
     *  Custom Modules
     */

], 'public_html/js/site.js');

if (mix.inProduction()) {
    mix.version();
}

mix.copy('resources/theme/coreui/src/img/', 'public_html/img');
mix.copy('resources/theme/coreui/src/js/', 'public_html/js');

mix.copy('resources/theme/custom/common/img/', 'public_html/img');
mix.copy('resources/theme/custom/common/js/', 'public_html/js');